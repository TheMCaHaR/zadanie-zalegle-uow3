package Checker;
public class CheckResult {
    private String message;
    private RuleResult result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RuleResult getResult() {
        return result;
    }

    public void setResult(RuleResult result) {
        this.result = result;
    }
}
