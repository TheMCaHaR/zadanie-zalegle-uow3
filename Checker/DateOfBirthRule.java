package Checker;

import domain.Person;

public class DateOfBirthRule implements CheckRule {
    @Override
    public CheckResult checkRule(Object o) {
        CheckResult result = new CheckResult();

        if( ((Person)o).getDateOfBirth() == null)
            result.setResult(RuleResult.Exception);
        // itd

        return result;
    }
}
