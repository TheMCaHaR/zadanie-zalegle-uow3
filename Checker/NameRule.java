package Checker;

import domain.Person;

public class NameRule implements CheckRule {

    @Override
    public CheckResult checkRule(Object o) {
        CheckResult result = new CheckResult();

        if(((Person) o).getFirstName() == null)
            result.setResult(RuleResult.Exception);
        else if(((Person) o).getFirstName() == "")
            result.setResult(RuleResult.Error);
        else
            result.setResult(RuleResult.Ok);
        return result;
    }
}
