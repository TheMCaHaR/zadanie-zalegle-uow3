package Checker;

import domain.Person;

public class PeselRule implements CheckRule {
    @Override
    public CheckResult checkRule(Object o) {
        CheckResult result = new CheckResult();

        if(((Person) o).getPesel() == null)
            result.setResult(RuleResult.Exception);
        else if(((Person) o).getPesel() == "")
            result.setResult(RuleResult.Error);
        else
            result.setResult(RuleResult.Ok);
        return result;
    }
}
