package Checker;

import domain.Person;
import domain.User;

public class EmailRule implements CheckRule {
    @Override
    public CheckResult checkRule(Object o) {
        CheckResult result = new CheckResult();

        if( ((Person)o).getEmail() == null)
            result.setResult(RuleResult.Exception);
        else if(((Person) o).getEmail() == "" || !((Person) o).getEmail().contains("@"))
            result.setResult(RuleResult.Error);
        else
            result.setResult(RuleResult.Ok);

        return result;
    }
}
