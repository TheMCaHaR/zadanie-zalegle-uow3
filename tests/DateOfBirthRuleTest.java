package tests;

import Checker.CheckRule;
import Checker.DateOfBirthRule;
import Checker.RuleResult;
import domain.Person;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Paul on 24.11.2015.
 */
public class DateOfBirthRuleTest {

    private CheckRule rule;
    @Before
    public void setUp() throws Exception {
        this.rule = new DateOfBirthRule();
    }

    @Test
    public void  testIfDateIsNullReturnsException() {
        Person person = new Person();

        RuleResult actual = this.rule.checkRule(person).getResult(),
                excepted = RuleResult.Exception;

        Assert.assertEquals(actual, excepted);
    }
}