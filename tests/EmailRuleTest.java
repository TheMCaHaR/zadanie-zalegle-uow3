package tests;

import Checker.CheckRule;
import Checker.EmailRule;
import Checker.PasswordRule;
import Checker.RuleResult;
import domain.Person;
import domain.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmailRuleTest {
    private CheckRule rule;
    @Before
    public void setUp() throws Exception {
        this.rule = new EmailRule();
    }

    @Test
    public void testIfEmailIsNullThrowsException() {
        Person user = new Person();

        RuleResult actual = this.rule.checkRule(user).getResult(),
                excepted = RuleResult.Exception;

        Assert.assertEquals(actual, excepted);
    }
    @Test
    public void testIfEmailIsEmptyReturnsError() {
        Person user = new Person();
        user.setEmail("");
        RuleResult actual = this.rule.checkRule(user).getResult(),
                excepted = RuleResult.Error;

        Assert.assertEquals(actual, excepted);
    }
    @Test
    public void testIfEmailDoesNotHaveAtCharacterReturnsError() {
        Person user = new Person();
        user.setEmail("bblabla.pl");
        RuleResult actual = this.rule.checkRule(user).getResult(),
                excepted = RuleResult.Error;

        Assert.assertEquals(actual, excepted);
    }
    @Test
    public void testIfEmailDoesHaveAtCharacterReturnsOk() {
        Person user = new Person();
        user.setEmail("blabla@bl.pl");
        RuleResult actual = this.rule.checkRule(user).getResult(),
                excepted = RuleResult.Ok;

        Assert.assertEquals(actual, excepted);
    }
}